setenv fdtfile 'allwinner/sun50i-a64-pinetab-early-adopter.dtb'
if test ${devnum} = 0; then
  setenv linux_devnum 0
else
  setenv linux_devnum 2
fi

setenv bootargs console=ttyS0,115200 consoleblank=0 systempart=/dev/disk/by-label/system datapart=/dev/disk/by-label/userdata security=apparmor quiet splash logo.nologo vt.global_cursor_default=0
echo "[UBPORTS] Bootargs are" ${bootargs}
echo "[UBPORTS] Beginning load"

if test "${volume_key}" = "up" -o -e mmc ${devnum}:3 reboot-recovery; then
    # recovery-a partition
    setenv partnum 6
    setenv fdtfile 'dtb'
else
    #boot-a partition
    setenv partnum 4
    setenv fdtfile "dtbs/${fdtfile}"
fi

if load mmc ${devnum}:${partnum} ${ramdisk_addr_r} vmlinuz; then
  echo "[UBPORTS] Compressed kernel loaded"
  if unzip ${ramdisk_addr_r} ${kernel_addr_r}; then
    echo "[UBPORTS] kernel uncompressed"
    if load mmc ${devnum}:${partnum} ${fdt_addr_r} "${fdtfile}"; then
      echo "[UBPORTS] dtb loaded"
      if load mmc ${devnum}:${partnum} ${ramdisk_addr_r} initrd.img; then
        echo "[UBPORTS] Booting kernel with initramfs"
        booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r};
      else
        echo "[UBPORTS] Refusing to boot kernel without initramfs"
        reset
      fi
    else
      echo "[UBPORTS] Failed to load dtb"
    fi
  else
    echo "[UBPORTS] Failed to unzip kernel"
  fi
else
  echo "[UBPORTS] Failed to load kernel"
fi

reset
