#!/bin/bash

set -xe

OUT="$(realpath "$1" 2>/dev/null || echo 'out')"
mkdir -p "$OUT"

TMP=$(mktemp -d)
HERE=$(pwd)
SCRIPT="$(dirname "$(realpath "$0")")"/build

mkdir "${TMP}/system"
mkdir "${TMP}/partitions"

TMPDOWN=$(mktemp -d)
cd "$TMPDOWN"
    wget -O 'linux-image-6.5.0-okpine-ut_6.5.0-1_arm64.deb' 'https://gitlab.com/ook37/linux/-/jobs/artifacts/okpine-6.5-ut/raw/linux-image-6.5.0-okpine-ut_6.5.0-1_arm64.deb?job=build_phone'
    wget -O 'kernel-okpine.gz' 'https://gitlab.com/ook37/jumpdrive-ubports2/-/jobs/artifacts/ubports-recovery/raw/kernel-okpine.gz?job=build'
    wget -O 'recovery-pinephone.img.xz' 'https://gitlab.com/ook37/jumpdrive-ubports2/-/jobs/artifacts/ubports-recovery/raw/recovery-pinephone.img.xz?job=build'
    wget -O 'recovery-pinetab.img.xz' 'https://gitlab.com/ook37/jumpdrive-ubports2/-/jobs/artifacts/ubports-recovery/raw/recovery-pinetab.img.xz?job=build'
    wget -O 'recovery-pinephone-pro.img.xz' 'https://gitlab.com/ook37/jumpdrive-ubports2/-/jobs/artifacts/ubports-recovery/raw/recovery-pinephone-pro.img.xz?job=build'
    wget -O 'recovery-pinetab2.img.xz' 'https://gitlab.com/ook37/jumpdrive-ubports2/-/jobs/artifacts/ubports-recovery/raw/recovery-pinetab2.img.xz?job=build'
    wget -O 'initrd.img-touch-arm64pinephone' 'https://gitlab.com/ook37/initramfs-tools-ubuntu-touch/-/jobs/artifacts/focal/raw/out/initrd.img-touch-arm64pinephone?job=build'
    wget -O 'initrd.img-touch-arm64pinetab' 'https://gitlab.com/ook37/initramfs-tools-ubuntu-touch/-/jobs/artifacts/focal/raw/out/initrd.img-touch-arm64pinetab?job=build'
    unxz 'recovery-pinephone.img.xz'
    unxz 'recovery-pinetab.img.xz'
    unxz 'recovery-pinephone-pro.img.xz'
    unxz 'recovery-pinetab2.img.xz'
    ls .
cd "$HERE"

"$SCRIPT/deb-to-bootimg.sh" "${TMPDOWN}/linux-image-*-okpine*.deb" "${TMPDOWN}/kernel-okpine.gz" "${TMPDOWN}/initrd.img-touch-arm64pinephone" "${TMP}/partitions/boot.img"
"$SCRIPT/mk-persist.sh" 'uboot' "${TMP}/partitions/"

cd "${HERE}"
"$SCRIPT/mk-scr.sh" 'pinephone' 'uboot' "${TMP}/partitions/"
touch "${TMP}/partitions/loader.img"
dd if=loaders/u-boot-sunxi-with-spl.bin of="${TMP}/partitions/loader.img" bs=128k seek=1 conv=fsync
cp "${TMPDOWN}/recovery-pinephone.img" "${TMP}/partitions/recovery.img"
cp -av overlay/pinephone/* "${TMP}/"
"$SCRIPT/build-tarball-mainline.sh" pinephone "${OUT}" "${TMP}"
rm -r "${TMP}/system"
rm -f "${TMP}/partitions/loader.img"

cd "${HERE}"
"$SCRIPT/mk-scr.sh" 'pinephone-pro' 'uboot' "${TMP}/partitions/"
touch "${TMP}/partitions/loader.img"
dd if=loaders/u-boot-rockchip-spi.bin of="${TMP}/partitions/loader.img" bs=32k seek=1 conv=fsync
cp "${TMPDOWN}/recovery-pinephone-pro.img" "${TMP}/partitions/recovery.img"
cp -av overlay/pinephone-pro/* "${TMP}/"
"$SCRIPT/build-tarball-mainline.sh" pinephone-pro "${OUT}" "${TMP}"
rm -r "${TMP}/system"
rm -f "${TMP}/partitions/loader.img"

"$SCRIPT/deb-to-bootimg.sh" "${TMPDOWN}/linux-image-*-okpine*.deb" "${TMPDOWN}/kernel-okpine.gz" "${TMPDOWN}/initrd.img-touch-arm64pinetab" "${TMP}/partitions/boot.img"

cd "${HERE}"
"$SCRIPT/mk-scr.sh" 'pinetab' 'uboot' "${TMP}/partitions/"
touch "${TMP}/partitions/loader.img"
dd if=loaders/u-boot-sunxi-with-spl.bin of="${TMP}/partitions/loader.img" bs=128k seek=1 conv=fsync
cp "${TMPDOWN}/recovery-pinetab.img" "${TMP}/partitions/recovery.img"
cp -av overlay/pinetab/* "${TMP}/"
"$SCRIPT/build-tarball-mainline.sh" pinetab "${OUT}" "${TMP}"
rm -r "${TMP}/system"
rm -f "${TMP}/partitions/loader.img"

cd "${HERE}"
"$SCRIPT/mk-scr.sh" 'pinetab2' 'uboot' "${TMP}/partitions/"
touch "${TMP}/partitions/loader.img"
dd if=loaders/u-boot-rockchip.bin of="${TMP}/partitions/loader.img" bs=32k seek=1 conv=fsync
cp "${TMPDOWN}/recovery-pinetab2.img" "${TMP}/partitions/recovery.img"
cp -av overlay/pinetab2/* "${TMP}/"
"$SCRIPT/build-tarball-mainline.sh" pinetab2 "${OUT}" "${TMP}"
rm -r "${TMP}/system"
rm -f "${TMP}/partitions/loader.img"

rm -r "${TMP}"
rm -r "${TMPDOWN}"

echo "done"
