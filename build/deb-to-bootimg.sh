#!/bin/bash

set -xe

source "$(dirname $0)/mk-loopdev.sh"

DEB=$(realpath $1)
KER=$(realpath $2)
INT=$(realpath $3)
OUT=$(realpath $4)

TMP=$(mktemp -d)
TMPMNT=$(mktemp -d)
HERE=$(pwd)

cd ${TMP}
ar x ${DEB}
tar xf data.tar.*
cd ${HERE}

truncate --size 100M ${OUT}
mkfs.ext4 ${OUT}

LOOPDEV=$(mount_loopdev "$OUT" "$TMPMNT")

sudo mv ${TMP}/boot/* ${TMPMNT}
sudo rm ${TMPMNT}/vmlinuz-*
sudo cp ${KER} ${TMPMNT}/vmlinuz
sudo mkdir -p ${TMPMNT}/dtbs/allwinner/
sudo mkdir -p ${TMPMNT}/dtbs/rockchip/
sudo mv ${TMP}/usr/lib/linux-image-*/allwinner/sun50i-a64-pine*.dtb ${TMPMNT}/dtbs/allwinner/
sudo mv ${TMP}/usr/lib/linux-image-*/rockchip/rk3*-pine*.dtb ${TMPMNT}/dtbs/rockchip/
sudo mv ${TMP}/lib/modules ${TMPMNT}/modules
sudo cp ${INT} ${TMPMNT}/initrd.img

cleanup_loopdev "$LOOPDEV"

echo "done"
